﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowResult : MonoBehaviour
{
    public Sprite GoodSpr;      //好的圖片
    public Sprite BadSpr;       //壞的圖片

    public void ChangeSprite ( bool result )
    {
        //更換好/壞的圖片
        GetComponent<SpriteRenderer> ().sprite = result ? GoodSpr : BadSpr;
        //播放動畫
        GetComponent<Animation> ().Play ();
    }
}
