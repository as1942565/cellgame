﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMoveScript : MonoBehaviour
{

    #region-Joycon相關設定
    /// <summary>
    /// Joycon數量
    /// </summary>
    private List<Joycon> joycons;

    // Values made available via Unity
    /// <summary>
    /// Joycon類比搖桿數值
    /// </summary>
    public float[] stick;

    /// <summary>
    /// 指定目前的搖桿號碼
    /// </summary>
    public int jc_ind = 0;

    public Vector3 accel;

    private Joycon j;
    #endregion

    /// <summary>
    /// 按鍵設定:向上
    /// </summary>
    public KeyCode UpKey;

    // <summary>
    /// 按鍵設定:向下
    /// </summary>
    public KeyCode DownKey;

    // <summary>
    /// 按鍵設定:向左
    /// </summary>
    public KeyCode LeftKey;

    // <summary>
    /// 按鍵設定:向右
    /// </summary>
    public KeyCode RightKey;

    // <summary>
    /// 按鍵設定:互動按鈕(攻擊等等)
    /// </summary>
    public KeyCode ActionKey;

    public KeyCode DashKey;

    /// <summary>
    /// 玩家面相方向的手(?)之類的物件，旋轉用
    /// </summary>
    public Transform HandObj;

    /// <summary>
    /// 玩家可互動(攻擊)的判定點位置
    /// </summary>
    public Transform AttackPoint;

    public float DashAttackRange;

    /// <summary>
    /// 玩家攻擊的範圍
    /// </summary>
    public float AttackRange;


    /// <summary>
    /// 是否為使用Joycon的模式
    /// </summary>
    public bool JoyconMode;

    /// <summary>
    /// 玩家傷害
    /// </summary>
    public float Dmg;

    /// <summary>
    /// 玩家移動速度
    /// </summary>
    public float Speed;

    public Rigidbody2D rigid;

    public float NowDirX;
    public float NowDirY;


    public bool isDashing;

    public bool isDrifting;

    public bool CanDash;

    public Image DizzImg;

    public float UpVar;
    public float LeftVar;

    public float dashRate;

    public GameObject CD;

    void Start()
    {
        CanDash = true;
        DizzImg.enabled = false;
        //this.Speed = this.Speed / 10;
        // get the public Joycon array attached to the JoyconManager in scene
        isDashing = false;
        isDrifting = false;
        //取得目前的所有搖桿資訊
        joycons = JoyconManager.Instance.j;
        accel = new Vector3(0, 0, 0);
        rigid = this.GetComponent<Rigidbody2D>();

        //如果取得的搖桿數不滿指定搖桿編號的話，視為無搖桿
        if (joycons.Count < jc_ind + 1)
        {
            JoyconMode = false;
        }
        else
        {
            JoyconMode = true;
            //綁定搖桿
            j = joycons[jc_ind];
        }


    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (StopCtl.self.StopBool || isDashing || isDrifting)
            return;

        //this.transform.position += new Vector3(LeftVar, UpVar, 0);
        this.rigid.velocity = new Vector2(LeftVar, UpVar);
        if (UpVar != 0 || LeftVar != 0)
        {
            NowDirX = LeftVar;
            NowDirY = UpVar;
            float angle = Mathf.Atan2(UpVar, LeftVar) * Mathf.Rad2Deg + 90;
            this.HandObj.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }


    }
    void Update()
    {
        if (StopCtl.self.StopBool)
            return;


        if (isDashing)
        {
            DashAttackFunc();
            return;
        }

        //搖桿橫向拿取時，左右搖桿會有上下顛倒的狀況，因此如果為左搖桿的話就要反轉數值
        float isReverse = 1;

        //如果為使用搖桿模式則綁定搖桿
        if (JoyconMode)
        {
            //j = joycons[jc_ind];
            accel = j.GetAccel();

            if (j.isLeft)
            {
                isReverse = -1;
            }

            //取得類比搖桿的數值
            stick = j.GetStick();
        }

        this.UpVar = 0;
        this.LeftVar = 0;


        //向上走
        if (Input.GetKey(this.UpKey) || (JoyconMode ? stick[0] * isReverse < 0 : false))
        {
            UpVar = this.Speed * Time.deltaTime;
        }

        //向下走
        if (Input.GetKey(this.DownKey) || (JoyconMode ? stick[0] * isReverse > 0 : false))
        {
            UpVar = UpVar = this.Speed * Time.deltaTime * -1;
        }

        //向左走
        if (Input.GetKey(this.LeftKey) || (JoyconMode ? stick[1] * isReverse < 0 : false))
        {
            LeftVar = this.Speed * Time.deltaTime * -1;
        }

        //向右走
        if (Input.GetKey(this.RightKey) || (JoyconMode ? stick[1] * isReverse > 0 : false))
        {
            LeftVar = this.Speed * Time.deltaTime;
        }


        //先指定動作(攻擊)按鈕為搖桿的下按鈕
        Joycon.Button atkBtn = Joycon.Button.DPAD_DOWN;

        Joycon.Button DashBtn = Joycon.Button.DPAD_RIGHT;

        //如果為左搖桿，則改為上按鈕
        if (JoyconMode && j.isLeft)
        {
            atkBtn = Joycon.Button.DPAD_UP;
            DashBtn = Joycon.Button.DPAD_LEFT;
        }

        //按下動作(攻擊)按鈕後，執行攻擊動作
        if (Input.GetKeyDown(this.ActionKey) || (JoyconMode && j.GetButtonDown(atkBtn)) || (accel.z < -5.5f))
        {
            //使用搖桿的話加入震動效果
            if (JoyconMode) j.SetRumble(160, 320, 0.3f, 400);
            AttackFunc();
        }

        if (CanDash && (Input.GetKeyDown(this.DashKey) || (JoyconMode && j.GetButtonDown(DashBtn))))
        {
            DashAttack();
        }
    }

    void DashAttack()
    {
        StartCoroutine(DashRecoverCoroutine());
        StartCoroutine(DashCoroutine());
    }

    IEnumerator DashRecoverCoroutine()
    {
        CanDash = false;
        //yield on a new YieldInstruction that waits for 5 seconds.
        CD.SetActive ( true );
        CD.GetComponent<Animation> ().Play ( "CD" );
        yield return new WaitForSeconds(3f);
        CanDash = true;
    }

    IEnumerator DashCoroutine()
    {
        this.rigid.velocity += (new Vector2(NowDirX * dashRate, NowDirY * dashRate));
        isDashing = true;
        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(0.2f);
        isDashing = false;
    }

    void DashAttackFunc()
    {
        //取得攻擊範圍內打到多少物件
        Collider2D[] TargetHit = Physics2D.OverlapCircleAll(this.AttackPoint.position, this.DashAttackRange);

        //一一篩選物件
        foreach (Collider2D item in TargetHit)
        {
            //如果為目標(器官)則傳訊息變更器官狀態
            if (item.tag == "P1" || item.tag == "P2")
            {
                if (item.tag == this.tag) continue;
                if (item.GetComponent<PlayerMoveScript>().isDrifting) continue;
                item.GetComponent<PlayerMoveScript>().isDrifting = true;
                item.GetComponent<PlayerMoveScript>().rigid.AddForce(new Vector2(NowDirX * 4, NowDirY * 4));
                item.GetComponent<PlayerMoveScript>().DrifitingFunction();
                //EffectPlayer.self.PlayEffect("Attack", false);
            }

        }
    }

    public void DrifitingFunction()
    {
        StartCoroutine(Recover());
    }

    IEnumerator Recover()
    {
        EffectPlayer.self.PlayEffect("Dizziness", false);
        DizzImg.enabled = true;
        if (JoyconMode) j.SetRumble(320, 320, 3f, 1700);
        yield return new WaitForSeconds(1.7f);
        isDrifting = false;
        DizzImg.enabled = false;
    }


    /// <summary>
    /// 攻擊Function
    /// </summary>
    void AttackFunc()
    {
        //取得攻擊範圍內打到多少物件
        Collider2D[] TargetHit = Physics2D.OverlapCircleAll(this.HandObj.position, this.AttackRange);

        //一一篩選物件
        foreach (Collider2D item in TargetHit)
        {
            //如果為目標(器官)則傳訊息變更器官狀態
            if (item.tag == "Organ")
            {
                item.GetComponent<OrganScript>().GetHitFunc(Dmg);
                EffectPlayer.self.PlayEffect("Attack", false);
            }

        }
    }

    /// <summary>
    /// 輔助顯示攻擊範圍使用
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        if (HandObj != null)
        {
            Gizmos.DrawWireSphere(this.HandObj.position, this.AttackRange);
        }
        if (AttackPoint != null)
        {
            Gizmos.DrawWireSphere(this.AttackPoint.position, this.DashAttackRange);
        }
    }

}
