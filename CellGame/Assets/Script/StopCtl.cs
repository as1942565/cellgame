﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopCtl : MonoBehaviour
{
    public bool StopBool;           //現在是否已暫停

    public static StopCtl self;
    private void Awake ()
    {
        self = this;
    }
}
