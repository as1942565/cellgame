﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateCtl : MonoBehaviour
{
    public Sprite [] sprite;

    private void Start ()
    {
        transform.GetChild ( 0 ).GetComponent<Image> ().sprite = sprite [ Random.Range ( 0 , 2 ) ];
        transform.GetChild ( 0 ).transform.localPosition += new Vector3 ( 0 , Random.Range ( -300f , 300f ) , 0 );
    }

    private void FixedUpdate ()
    {
        transform.Rotate ( 0 , 0 , 3f );
    }
}
