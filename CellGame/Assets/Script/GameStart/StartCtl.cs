﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartCtl : MonoBehaviour
{
    private bool IsStart;       //是否開始遊戲

    private void Update ()
    {
        if ( Input.anyKeyDown && !IsStart || Input.GetMouseButtonDown ( 0 ) && !IsStart )
        {
            EffectPlayer.self.PlayEffect ( "Click" , false );
            IsStart = true;     //已開始遊戲
            LoadSceneCtl.self.GetComponent<Animation> ().Play ( "FadeOut" );    //畫面淡出
            LoadSceneCtl.self.SceneName = "SwitchActor";                        //0.5秒後切換場景
            LoadSceneCtl.self.WaitForChangeScene ( 0.5f );                      //0.5秒後切換場景
            BGMPlayer.self.PlayBGM ( "SwitchActor" );
        }
    }
}
