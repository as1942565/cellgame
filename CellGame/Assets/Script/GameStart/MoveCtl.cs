﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCtl : MonoBehaviour
{
    public GameObject Move;

    private void Start ()
    {
        Invoke ( "Add" , 1f );
    }

    public void Add ()
    {
        GameObject move = Instantiate ( Move , transform ) as GameObject;
        Destroy ( move, 13f );
        Invoke ( "Add", Random.Range ( 1f, 3f ) );
    }
}
