﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public string [] ActorName;         //腳色名稱  
    public bool [] OKBool;              //是否已決定
    public bool UseJoyCon;              //是否使用JoyCon遊玩

    public static PlayerData self;
    private void Awake ()
    {
        self = this;
    }
}
