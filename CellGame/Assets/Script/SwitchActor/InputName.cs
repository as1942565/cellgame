﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputName : MonoBehaviour
{
    public int PlayerID;        //玩家編號(預設左邊0 右邊1)
    public Text DefaultText;    //輸入文字前的提示
    public Text Actor1Name;     //輸入的文字
    public Text Actor2Name;     //輸入的文字
    public GameObject OK;       //已決定的圖示

    public void OKBtn ()
    {
        //決定後記錄玩家名稱
        PlayerData.self.ActorName [0] = Actor1Name.text;
        PlayerData.self.ActorName [1] = Actor2Name.text;
        //點擊音效
        EffectPlayer.self.PlayEffect ( "Click" , false );
        /*//N號玩家已決定
        PlayerData.self.OKBool [PlayerID] = true;
        //N號玩家已決定
        PlayerOK.self.SwitchActorOK = true;
        //增加一秒冷卻
        PlayerOK.self.MaxOKCD = Time.time + 1;
        //出現已決定圖示
        OK.SetActive ( true );
        //播放音效
        OK.GetComponent<Animation> ().Play ( "OK" );*/
        LoadSceneCtl.self.SceneName = "SwitchControl";
        LoadSceneCtl.self.GetComponent<Animation> ().Play ( "FadeOut" );
        LoadSceneCtl.self.WaitForChangeScene ( 1f );
    }

    public void CancelBtn ()
    {
        //點擊音效
        EffectPlayer.self.PlayEffect ( "Click" , false );
        //N號玩家取消決定
        PlayerData.self.OKBool [PlayerID] = false;
        //不會換場景
        PlayerOK.self.SwitchActorOK = false;
        //增加1秒冷卻
        PlayerOK.self.MaxOKCD = Time.time + 1;
        //取消OK圖示
        OK.SetActive ( false );
    }
}
