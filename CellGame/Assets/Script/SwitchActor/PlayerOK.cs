﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOK : MonoBehaviour
{
    public bool SwitchActorOK;
    public float MaxOKCD;

    public static PlayerOK self;
    private void Awake ()
    {
        self = this;
    }

    void Update()
    {
        if ( MaxOKCD < Time.time && SwitchActorOK )     //1秒後偵測是否都已決定
        {
            for ( int i = 0; i < PlayerData.self.OKBool.Length; i++  )
            {
                if ( !PlayerData.self.OKBool [i] )
                {
                    SwitchActorOK = false;              //其中一人尚未決定就是還沒決定
                    return;
                }
            }

            if ( SwitchActorOK )                        //如果都決定
            {
                //換場景
                LoadSceneCtl.self.SceneName = "Scene1";
                LoadSceneCtl.self.GetComponent<Animation> ().Play ( "FadeOut" );
                LoadSceneCtl.self.WaitForChangeScene ( 1f );
                BGMPlayer.self.PlayBGM ( "Main" );
                SwitchActorOK = false;
            }
        }
    }
}
