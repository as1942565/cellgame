﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchControl : MonoBehaviour
{
    public void Switch ( bool usejoycon )
    {
        EffectPlayer.self.PlayEffect ( "Click" , false );
        PlayerData.self.UseJoyCon = usejoycon;
        LoadSceneCtl.self.SceneName = "Scene1";
        LoadSceneCtl.self.GetComponent<Animation> ().Play ( "FadeOut" );
        LoadSceneCtl.self.WaitForChangeScene ( 1f );
        BGMPlayer.self.PlayBGM ( "Main" );
    }

    /// <summary>
    /// Joycon數量
    /// </summary>
    private List<Joycon> joycons;

    private Joycon j;

    /// <summary>
    /// 是否為使用Joycon的模式
    /// </summary>
    public bool JoyconMode;

    // Values made available via Unity
    /// <summary>
    /// Joycon類比搖桿數值
    /// </summary>
    public float[] stick;

    private bool JoyconIsMove;

    private bool IsUseJoyconMove;

    public Animation JoyCon;
    public Animation KeyBoard;
    private bool Usebool = true;

    private void Start()
    {
        JoyconIsMove = false;
        IsUseJoyconMove = false;
        //取得目前的所有搖桿資訊
        joycons = JoyconManager.Instance.j;

        //如果取得的搖桿數不滿指定搖桿編號的話，視為無搖桿
        if (joycons.Count < 1)
        {
            JoyconMode = false;
        }
        else
        {
            JoyconMode = true;
            //綁定搖桿
            j = joycons[0];
        }
    }

    private void Update ()
    {
        if (JoyconMode)
        {
            stick = j.GetStick();

            if (!JoyconIsMove&&(stick[1] < 0 || stick[1] > 0))
            {
                JoyconIsMove = true;
            }

            if (JoyconIsMove && (stick[1] ==0))
            {
                JoyconIsMove = false;
                IsUseJoyconMove = false;
            }
        }

        

        if ( Input.GetKeyDown ( KeyCode.RightArrow ) || Input.GetKeyDown ( KeyCode.LeftArrow )||(JoyconIsMove && !IsUseJoyconMove))
        {
            IsUseJoyconMove = true;
            Usebool = !Usebool;
            JoyCon.Stop ();
            KeyBoard.Stop ();
            if ( Usebool )
            {
                JoyCon.Play ( "Use" );
                KeyBoard.Play ( "NoUse" );
            }
            else
                {
                JoyCon.Play ( "NoUse" );
                KeyBoard.Play ( "Use" );
            }
        }

        if ( Input.GetKeyDown ( KeyCode.Return ) 
            || (JoyconMode && j.GetButtonDown(Joycon.Button.DPAD_DOWN))
            || (JoyconMode && j.GetButtonDown(Joycon.Button.DPAD_UP))
            || (JoyconMode && j.GetButtonDown(Joycon.Button.DPAD_LEFT))
            || (JoyconMode && j.GetButtonDown(Joycon.Button.DPAD_RIGHT)))
        {
            Switch ( Usebool );
        }
    }
}
