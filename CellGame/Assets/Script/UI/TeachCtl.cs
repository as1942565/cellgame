﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeachCtl : MonoBehaviour
{
    public Animation CountDown;
    public GameObject JoyCon;
    public GameObject KeyBoard;

    private void Start ()
    {
        JoyCon.SetActive ( PlayerData.self.UseJoyCon );
        KeyBoard.SetActive ( !PlayerData.self.UseJoyCon );
    }

    private void Update ()
    {
        if ( Input.anyKey )
        {
            gameObject.SetActive ( false );
            CountDown.Play ( "CountDown" );
            StopCtl.self.StopBool = false;
        }
    }
}
