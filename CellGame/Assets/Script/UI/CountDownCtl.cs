﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDownCtl : MonoBehaviour
{
    private float TotalTime = 20f;
    
    public void CountDown ()
    {
        TotalTime -= 1;
        GetComponent<Text> ().text = TotalTime.ToString ();
        if ( TotalTime <= 0 )
        {
            StopCtl.self.StopBool = true;
            GetComponent<Animation> ().Stop ();
            BGMPlayer.self.GetComponent<AudioSource> ().Stop ();
            EffectPlayer.self.PlayEffect ( "TimesUp" , false );
            MenuCtl.self.Result ();
        }
        if ( TotalTime <= 5 && TotalTime > 0 )
            EffectPlayer.self.PlayEffect ( "Warning" , false );
    }
}
