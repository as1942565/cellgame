﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Opening : MonoBehaviour
{
    public GameObject UI;
    public GameObject Lung;
    public GameObject P2;

    public GameManager GM;

    public Text [] PlayerName;
    public Text [] NameBack;

    private void Start ()
    {
        this.GM = GameObject.Find("GameManager").GetComponent<GameManager>();
        Lung.SetActive ( false );
        StopCtl.self.StopBool = true;
        for ( int i = 0; i < PlayerName.Length; i++ )
        {
            if ( PlayerData.self.ActorName [i] == "" )
                PlayerData.self.ActorName [i] = "NoName";
            PlayerName [i].text = PlayerData.self.ActorName[i];
            NameBack [i].text = PlayerData.self.ActorName[i];
        }
        if ( PlayerData.self.ActorName [0] == "武漢肺炎" )
        {
            P2.GetComponent<CircleCollider2D> ().radius = 3;
            P2.GetComponent<PlayerMoveScript> ().AttackRange = 4.5f;
        }
    }

    public void GameStart ()
    {
        UI.SetActive ( true );
        //StopCtl.self.StopBool = false;
    }

    public void Show ()
    {
        Lung.SetActive ( true );
        GM.InitFunc();
        GM.CalculateScore();
       
    }
}
