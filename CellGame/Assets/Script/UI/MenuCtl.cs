﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuCtl : MonoBehaviour
{
    public static MenuCtl self;
    private void Awake ()
    {
        self = this;
    }
    public Animation menu;
    public Text P1result;
    public Text P2result;
    public GameManager GM;

    public void Result ()
    {
        menu.Play ( "MenuResult" );
        P1result.text = ( (int)GM.P2Percent ).ToString ();
        P2result.text = ( (int)GM.P1Percent ).ToString ();
    }

    public void ChangeScene ( string SceneName )
    {
        if (SceneName == "Scene1")
        {
            BGMPlayer.self.PlayBGM ( "Main" );
        }
        else
        {
            BGMPlayer.self.PlayBGM ( "GameStart" );
        }

        //List<Joycon>joycons = JoyconManager.Instance.j;

        //for (int i = 0; i < joycons.Count; ++i)
        //{
        //    joycons[i].Detach();
        //}
        //Destroy(JoyconManager.Instance.gameObject);
        LoadSceneCtl.self.SceneName = SceneName;
        LoadSceneCtl.self.WaitForChangeScene ( 0.5f );
        LoadSceneCtl.self.GetComponent<Animation> ().Play ( "FadeOut" );
    }
}
