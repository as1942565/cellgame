﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneCtl : MonoBehaviour
{
    public string SceneName;        //需要切換的場景名稱

    public static LoadSceneCtl self;
    private void Awake ()
    {
        self = this;
    }

    public void WaitForChangeScene ( float delay )
    {
        Invoke ( "GoToScene" , delay );         //幾秒後切換場景
    }

    public void GoToScene ()
    {
        SceneManager.LoadScene ( SceneName );    //切換場景
    }
}
