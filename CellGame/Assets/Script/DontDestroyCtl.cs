﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyCtl : MonoBehaviour
{
    public GameObject [] DontDestroyObjs;

    private static bool OnceBool = false;

    void Awake ()
    {
        if ( !OnceBool )
        {
            for ( int i = 0; i < DontDestroyObjs.Length; i++ )
            {
                GameObject obj = Instantiate ( DontDestroyObjs[i] ) as GameObject;
                DontDestroyOnLoad ( obj );
            }
            OnceBool = true;
        }
    }
}
