﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrganScript : MonoBehaviour
{    
    public float Health;
    public GameObject Prompt;
    private GameObject TempPrompt;
    private bool PromptBool;

    private GameManager GM;

    // Start is called before the first frame update
    void Start()
    {
        GM = GameObject.Find("GameManager").GetComponent<GameManager>();
        SetStateSprite();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GetHitFunc(float point)
    {
        //Debug.Log(this.name + " Hit!");
        Health += point;
        SetStateSprite();
        GM.CalculateScore();
    }

    public void SetStateSprite()
    {
        Health = Mathf.Clamp ( Health , 0 , 10 );
        GetComponent<SpriteRenderer> ().color = new Color32 ( 255 , 255 , 255 , byte.Parse ( ( ( Health + 1 ) * 23 ).ToString() )  );
        if ( Health >= 10 )
            transform.GetChild( 0 ).GetComponent<ShowResult> ().ChangeSprite ( false );
        else if ( Health <= 0 )
            transform.GetChild( 0 ).GetComponent<ShowResult> ().ChangeSprite ( true  );

        /*if (Health <= 0)
        {
            this.GetComponent<SpriteRenderer>().sprite = this.P2Sprite;
            Health = 0;
        }
        if (Health >= 10)
        {
            this.GetComponent<SpriteRenderer>().sprite = this.P1Sprite;
            Health = 10;
        }*/
    }

    private void OnTriggerEnter2D ( Collider2D collision )
    {
        if ( collision.gameObject.tag == "P1" || collision.gameObject.tag == "P2" && !PromptBool )
        {
            PromptBool = true;
            TempPrompt = Instantiate ( Prompt , transform.localPosition , transform.rotation ) as GameObject;
            TempPrompt.GetComponent<Animation> ().Play ( "Prompt" );
        }
    }

    private void OnTriggerExit2D ( Collider2D collision )
    {
        if ( collision.gameObject.tag == "P1" || collision.gameObject.tag == "P2" )
        {
            PromptBool = false;
            if (TempPrompt != null)
                TempPrompt.GetComponent<Animation> ().Play ( "Prompt2" );
            Destroy ( TempPrompt , 1.0f );
        }
    }

}
