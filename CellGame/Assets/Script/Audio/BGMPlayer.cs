﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMPlayer : MonoBehaviour
{
    public float v;

    public static BGMPlayer self;
    private void Awake ()
    {
        self = this;
    }

    AudioSource AS;
    private const string BGMPath = "Audios/BGM/";

    private void Start ()
    {
        AS = GetComponent<AudioSource> ();
        PlayBGM ( "GameStart" );
    }

    public void PlayBGM ( string name )
    {
        AS.volume = v;
        AS.clip = Resources.Load<AudioClip> ( BGMPath + name );
        AS.Play ();
    }
}
