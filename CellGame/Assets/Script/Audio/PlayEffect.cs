﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayEffect : MonoBehaviour
{
    AudioSource AS;
    private bool DeleteBool;

    private void Start ()
    {
        AS = GetComponent<AudioSource> ();
        Invoke ( "Delete" , 1f );
    }

    void Delete ()
    {
        DeleteBool = true;
    }

    private void Update ()
    {
        if ( !AS.isPlaying && DeleteBool )
            Destroy ( gameObject );
    }
}
