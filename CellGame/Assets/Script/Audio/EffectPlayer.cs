﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectPlayer : MonoBehaviour
{
    public float v;

    public static EffectPlayer self;
    private void Awake ()
    {
        self = this;
    }

    public GameObject PlayEffectObj;
    
    private const string EffectPath = "Audios/Effect/";

    public void PlayEffect ( string path , bool loop )
    {
        AudioSource AS = Instantiate ( PlayEffectObj ).GetComponent<AudioSource> ();
        AS.volume = v;
        AS.clip = Resources.Load<AudioClip> ( EffectPath + path );
        AS.loop = loop;
        AS.Play ();
        AS.GetComponent<PlayEffect> ().enabled = true;
    }
}
