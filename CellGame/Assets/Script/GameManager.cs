﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public float P1Percent;
    public float P2Percent;

    public Text P1PercentTxt;
    public Text P2PercentTxt;

    public List<GameObject> OrganList;

    public int OrganNumberSetting;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitFunc()
    {
        List<int> GetRandNum = new List<int>();
        GetRandNum = RandNum();
        foreach (var item in OrganList)
        {
            item.SetActive(false);
        }
        foreach (var item in GetRandNum)
        {
            OrganList[item].SetActive(true);
            OrganScript org = OrganList[item].GetComponent<OrganScript>();
            org.Health = 5;
        }

        //List<Joycon> joycons = JoyconManager.Instance.j;

        //for (int i = 0; i < joycons.Count; ++i)
        //{
        //    joycons[i].Detach();
        //}
    }

    private List<int> RandNum()
    {
        List<int> Rslt = new List<int>();
        List<int> NumList=new List<int>();
        for (int i = 0; i < OrganList.Count; i++)
        {
            NumList.Add(i);
        }
        int Cnt = OrganList.Count;
        for (int i = 0; i < OrganNumberSetting; i++)
        {
            int tmpNum = Random.Range(0, Cnt);
            Rslt.Add(NumList[tmpNum]);

            int tmp = NumList[tmpNum];
            NumList[tmpNum] = NumList[Cnt - 1];
            NumList[Cnt - 1] = tmp;
            Cnt--;
        }
        
        return Rslt;
    }

    public void CalculateScore()
    {
        GameObject[] OrganList = GameObject.FindGameObjectsWithTag("Organ");

        float SumVar = OrganList.Length*10;
        float TmpVar = 0;
        foreach (var item in OrganList)
        {
            TmpVar += item.GetComponent<OrganScript>().Health;
        }

        P1Percent = (TmpVar / SumVar)*100;
        P2Percent = 100 - P1Percent;

        P1PercentTxt.text = P1Percent + "%";
        P2PercentTxt.text = P2Percent + "%";
    }
}
